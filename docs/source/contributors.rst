Student Contributors
====================

A number of students have worked on the SEUP project in varying capacity and time period (few weeks to 1-2 years)

University of Wisconsin-Madison
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(In no particular order)

* Zeng Fan
* Matthew Wawiorka
* Lee Shaver
* Travis Leanna
* Ben Chylla
* Sam Singer
* Alec Sivit
* Morgan Zhang
* Ashray Manur
* David Sehloff 
* Adria Brooks 
* Andrew Gilles 
* Raul Martins
* Maitreyee Marathe


National Institute of Engineering 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(In no particular order)

* Abhishek R
* Sindhu Dixith
* Vinay Joshi
* Anup Athreya 
* Ansu Alex
* Akshay VJ
* Deepak G
* Dhananjaya KN
* NIE Microgid Phase 1-4 student teams 


Others
^^^^^^
(In no particular order)

* Anna Lunes
* Nicole Bugay