Developing Energy Apps
======================

You can develop and deploy energy apps on HEM or on MEMCloud. 
For latency sensitive applications, apps can be deployed on HEM. 
Microgrid-level and computationally intensive applications can be deployed on MEMCloud